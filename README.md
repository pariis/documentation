# Project: Paris
Author: Jeroen Vesseur

## Introduction
The paris project is based on the machine learning autoencoder concept. 

An autoencoder is a neural network that learns to copy its input to its output. It has an internal (hidden) layer that describes a code used to represent the input, and it is constituted by two main parts: an encoder that maps the input into the code, and a decoder that maps the code to a reconstruction of the input.
## The autoencoder
This project is based on the I think already pretty famous Sketch-RNN model implementation first describe by David Ha and Doublas Eck in therey paper [A Neural Representation of Sketch Drawings](https://arxiv.org/abs/1704.03477). A well known implementation from the [Google Magenta](https://magenta.tensorflow.org/sketch-rnn-demo) team seems to be quite popular and is very informative if you seek to get more in depth knowledge of the sketch-rnn model and its possible usage. Also the sketch-rnn model has been incorporated into the creatiove [ml5.js](https://ml5js.org) framework.

The basic machine learning model wether it is an cnn or rnn is ofcourse still the autoencoder. The autoencoder principle is based like the principal component analysis (PCA) on dimensionality reduction. An input feature set is transformed step by step into a smaller feature set by an encoder and then gradually enlarged by a decoder with the purpose of recreating the input 'as close as possible'. See the schematic overview of this architecture in figure 1. 

![Autoencoder architecture](Docs/Autoencoder_architecture.png)
*Figure1 Autoencoder basic architecture*

The encoder has the input layer for input x with usually the number of features as the number of input nodes and incrementally scales down the feature data by adding layers with reduced number of nodes until we reach the shared latent space. The latent space contains the least number of nodes and this latent space represents the most import features of the input whithout the noise and the redundant data etc. This latent space layer is the shared layer between the encoder and the decoder. Then from the latent space the encoder model will add layers with increasing number of nodes until we reach the same dimensions of the input layer in the output layer x'. The goal of the training of this autoencoder is to have x' resemble x as closely as possible. 

## Latent space sampling
The interesting part for me is the fact that once the model is trained and is able to reproduce the trained input, the encoder part can be removed and the model can be used as a decoder that will try to reproduce x inputs from sampling of the latent space. 

Sampling of the latent space will produce features closely related to the input features. But what happens when you feed the model (encoder) with data from a completely different domain (here we go again), for instance twitter data?

Can you use this system to generate or translate or visualise data from one domain into another domain?

## SVG data vs Pixel data
For this project instead of generating pixel based images like in my previous projects [Helena](https://helena.wvinull.com/Helena) and [WeSeeYou](https://gitlab.com/weseeyou/documentation/-/blob/4ec0aca8d7b97c68a3ab086d2a4e708f543b9b77/Readme.md) this time I wanted to generate SVG image data that can then be plotted with the AxiDraw plotter.


# The components
# The model
For this project I completely refacted the sketc-rnn model from stwind for my own purposes. The new implementation can be found [here](App/Models/SVGAutoEncoder.py)

# SVG training data
To train the model I use the [quickdraw datasets](https://console.cloud.google.com/storage/browser/quickdraw_dataset/sketchrnn) provided by google as part of their I believe Magenta project. Because I am still building my own dataset I train my model a number of epochs on a specific quickdraw dataset for example the Aaron Sheep data set and  to get the model in an initial state and then continue training on my own data. 

The [data](App/Data) folder contains two directories with training data for the RNN model. 

- Custom: the train, validate and test data directories with my own hand drawn SVG images. To convert these images in these directories into compressed and optimized numpy arrays for training the model you can use the test_build_dataset method in the TestDataProcessorCustom class.

    python -m pytest -s Tests/test_DataProcessorCustom.py

This test method will take the train, validate and test data and transforms it into normalized stroke3 formatted numpy arrays and stores them in the file Data/Custom/spawn.npz
 
- QuickDraw: in this directory you will find the [quickdraw curated files](https://console.cloud.google.com/storage/browser/quickdraw_dataset/sketchrnn) per subject from the Google research team and the [Aaron Koblin Sheep Dataset](http://www.aaronkoblin.com/project/the-sheep-market)

# SVG 
The sketch-rnn model is based around the Scalable Vector Graphic (SVG) format of two dimensional images. In contrast to pixel or raster based images where pixels are defined in a fixed grid following the ARGB principles. An SVG image is based on a finite set of paths and shapes defined as interconnected points on a cartesian plane. Let me illustrate.

Below you see Mario twice. Mario on the left is drawn in a bitmap pixel system. Mario on the right is drawn in a vector grid.
<p align="center">
    <img width="300" height="300" src="./Images/mariobmpvssvg.png">
</p>

Bitmaps are predefined grids of pixels and therefore do not scale wel. Imagine a straight line in a 4 x 4 grid.


    0 0 0 0
    - - - - 
    0 0 0 0 
    0 0 0 0
    
If you would scale up this image 2 times you would either get holes in the grid (unless you create some smart enlargment algorithm) and the image quality would change. You will loose information.
    
    0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0
    - 0 - 0 - 0 - 0
    0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0
    0 0 0 0 0 0 0 0

Vector images like SVG define lines by start and end points and the transition between these points. That same line is defined in a vector format like this.

    <line
        x1="0"
        y1="1"
        x2="4"
        y2="1"
     />

If you would scale up this image 2 times you would end up with something like this.

    <line
        x1="0"
        y1="2"
        x2="8"
        y2="2"
     />

Another way to describe an SVG line is with the path command which is actually the same as the line command above but is more convenient to turn into data that can be fed into a machine learning model. More on that in a moement. 

The same line in a path format looks like this.

     <path d="M 0 1 L 4 1">

The SVG path syntax closely resembles the pen plot commands the M commands represents a Move or MoveTo command. In this case you 'Move' a 'Pen' to position (0, 1) on the cartesian plane. The L commands indicates that we need to 'draw' a line to the next position (4, 1).

Other interesting SVG commands are C for Cubic Bezier and Q for Quadratic Bezier. Take a look at one of the [official SVG references](https://svgwg.org/specs/paths/) for more in depth details.

## Strokes (todo)

## Stroke3 vs Stroke5 (todo)


# Training
## Docker
The docker image is located [here](https://hub.docker.com/repository/docker/spawnwvinull/paris). 

Use the docker-compose.yml to start up the container.

    docker-compose -f docker-compose.yml run parisApp

or 

    docker-compose -f docker-compose.yml up

and then

    docker exec -it <image_name> /bin/bash

## Training the model
Then within the container you can start training the model on custom or quickdraw data. Or you can continue training based on an pre-trained model using a pre-trained weights file.

Use the python TrainModel.py for this.

    usage: TrainModel.py [-h] -c Path -f FileName [-w Filename] -p Path

    Train the model

    optional arguments:
    -h, --help          show this help message and exit
    -c Directory, --checkpointdir Directory
                        The directory to store the checkpoint files to.
    -f FileName, --datafile FileName
                        The npz file name with the commpressed training data.
    -w FileName, --weightsfile FileName
                        The pre trained weights file for the model.
    -p FileName, --hyperparameterfile FileName
                        The hyperparameter file for the model.

For example: Custom

    python TrainModel.py -c Output/Checkpoints/Custom -f Data/Custom/spawn.npz -p Data/Hyperparameters/default.spawn.hps

For example: QuickDraw

    python TrainModel.py -c Output/Checkpoints/Sheep -f Data/QuickDraw/aaron_sheep.npz -p Data/Hyperparameters/default.sheep.hps

For local testing: with single epoch

    python TrainModel.py -c Output/Checkpoints/Quickdraw -f Data/QuickDraw/aaron_sheep.npz -p Data/Hyperparameters/default.sheep.singleEpoch.hps

For example: Continue training a model from pre trained weights.
    
    python TrainModel.py -c Output/Checkpoints/Experiment3 -f Data/Custom/spawn.npz -w Data/PreTrained/Sheep/Model_Weights.50_0.36.hdf5 -p Data/Hyperparameters/default.spawn.hps

# Drawing
## Generate TfLite model (model, pretrained modelstate and hyperparameters)
At the end of model training in the checkpoint directory (-c entry) a tflite direcotry is created with the tflite model, the tflite initial model state and the hyperparameters used for the training of this model.

These three items need to be provided to the DrawMachine for inference or drawing in this case.

    /tflite/
           /model.tflite
           /modelInitialState.tflite
           /hyperparamters.hps

## Draw output from latent space
Once you have a properly trained model you can sample the latent space to generate images or figures. The Draw component generates an svg file from the latent space and is capable of sending it to an AxiDraw plotter.

## env
Start the virtual environment. 

    source env/bin/activate

## Regular Draw usage
Use the python `Draw.py` for to draw.

    usage: Draw.py [-h] -m Filename -i Filename -p FileName -o FileName [-n Int]                               
               [-l Int Int] [-s Int Int] [-f Float] [-r Boolean]

    Draw from latent space

    optional arguments:
    -h, --help            show this help message and exit
    -m Filename, --modelfile Filename
                            The pretrained tflite model for inference.
    -i Filename, --initialstatemodelfile Filename
                            The pretrained initial state tflite model for
                            inference.
    -p FileName, --hyperparameterfile FileName
                            The hyperparameter file for the model.
    -o FileName, --outputfile FileName
                            The svg output file as the draw result.
    -n Int, --numberofshapes Int
                            The number of shapes to put on a layer.
    -l Int Int, --layerposition Int Int
                            The top left start position for the layer.
    -s Int Int, --layersize Int Int
                            The widht x height of the layer.
    -f Float, --factor Float
                        The scaling factor
    -r Boolean, --preview Boolean
                            Preview will simulate the AxiDraw plotter commands. If
                            you actually want to plot with the AxiDraw plotter use
                            False. Default is True.
    

For example:

    python Draw.py -m Data/Demo/demo_tflitemodel.tflite -i Data/Demo/demo_tflitemodel_initial_state.tflite -p Data/Demo/demo_hyperparameters.hps -o Output/demo_2_draw.svg

or

    python Draw.py -m Data/Demo/demo_tflitemodel.tflite -i Data/Demo/demo_tflitemodel_initial_state.tflite -p Data/Demo/demo_hyperparameters.hps -o Output/demo_1_draw.svg -l 80 80 -s 800 880 -n 8

## Draw demo usage
For demo purposes there is a simple version available use `DrawDemo.py` for this.

    usage: DrawDemo.py [-h] [-n Int] [-f Float] [-r Boolean]

    Draw from latent space (demo)

    optional arguments:
    -h, --help            show this help message and exit
    -n Int, --numberofshapes Int
                            The number of shapes to put on a layer.
    -f Float, --factor Float
                        The scaling factor
    -r Boolean, --preview Boolean
                            Preview will simulate the AxiDraw plotter commands. If
                            you actually want to plot with the AxiDraw plotter use
                            False. Default is True.

For example:

    python DrawDemo.py -n 10 -r True

## More usage examples
Draw sheep figures in different frames.

| Keyword           | Value                   |
|-------------------|-------------------------|
| Subject           | Sheep                   |
| Model             | Data/Sheep/model.tflite |
| Output            | Output/sheep.svg        |
| NumberOf items    | 8                       |
| Top left x        | 80                      |
| Top left y        | 80                      |
| Width             | 800                     |
| Height            | 880                     |

    python Draw.py -m Data/Sheep/model.tflite -i Data/Sheep/modelInitialState.tflite -p Data/Sheep/hyperparamters.hps -o Output/sheep.svg -l 80 80 -s 800 880 -n 8

| Keyword           | Value                   |
|-------------------|-------------------------|
| Subject           | Cats                    |
| Model             | Data/Sheep/model.tflite |
| Output            | Output/cats.svg         |
| NumberOf items    | 16                      |
| Top left x        | 100                     |
| Top left y        | 100                     |
| Width             | 800                     |
| Height            | 1280                    |
| Scaling factor    | 0.0001                  |

    python Draw.py -m Data/Cats/model.tflite -i Data/Cats/modelInitialState.tflite -p Data/Cats/hyperparamters.hps -o Output/cats.svg -l 100 100 -s 800 1280 -n 16 -f 0.0001

| Keyword           | Value                       |
|-------------------|-----------------------------|
| Subject           | Diamonds                    |
| Model             | Data/Diamonds/model.tflite  |
| Output            | Output/diamonds.svg         |
| NumberOf items    | 16                          |
| Top left x        | 100                         |
| Top left y        | 100                         |
| Width             | 800                         |
| Height            | 1280                        |
| Scaling factor    | 0.00009                      |

    python Draw.py -m Data/Diamond/model.tflite -i Data/Diamond/modelInitialState.tflite -p Data/Diamond/hyperparamters.hps -o Output/diamonds.svg -l 100 100 -s 800 1280 -n 16 -f 0.00009

| Keyword           | Value                         |
|-------------------|-------------------------------|
| Subject           | Spawn mixed with Sheep        |
| Model             | Data/SpawnSheep/model.tflite  |
| Output            | Output/spawn_sheep.svg        |
| NumberOf items    | 16                            |
| Top left x        | 100                           |
| Top left y        | 100                           |
| Width             | 800                           |
| Height            | 1280                          |
| Scaling factor    | 0.0001                        |

    python Draw.py -m Data/SpawnSheep/model.tflite -i Data/SpawnSheep/modelInitialState.tflite -p Data/SpawnSheep/hyperparamters.hps -o Output/spawn_sheep.svg -l 100 100 -s 800 1280 -n 16 -f 0.0001

| Keyword           | Value                       |
|-------------------|-----------------------------|
| Subject           | Spawn mixed with Cats       |
| Model             | Data/SpawnCats/model.tflite |
| Output            | Output/spawn_cats.svg       |
| NumberOf items    | 16                          |
| Top left x        | 100                         |
| Top left y        | 100                         |
| Width             | 800                         |
| Height            | 1280                        |
| Scaling factor    | 0.001                      |

    python Draw.py -m Data/SpawnCats/model.tflite -i Data/SpawnCats/modelInitialState.tflite -p Data/SpawnCats/hyperparamters.hps -o Output/spawn_cats.svg -l 100 100 -s 800 1280 -n 16 -f 0.001

| Keyword           | Value                           |
|-------------------|---------------------------------|
| Subject           | Spawn mixed with Diamonds       |
| Model             | Data/SpawnDiamonds/model.tflite |
| Output            | Output/spawn_diamonds.svg       |
| NumberOf items    | 16                              |
| Top left x        | 100                             |
| Top left y        | 100                             |
| Width             | 800                             |
| Height            | 1280                            |
| Scaling factor    | 0.0008                          |

    python Draw.py -m Data/SpawnDiamonds/model.tflite -i Data/SpawnDiamonds/modelInitialState.tflite -p Data/SpawnDiamonds/hyperparamters.hps -o Output/spawn_diamonds.svg -l 100 100 -s 800 1280 -n 16 -f 0.0008

# Credits
https://github.com/stwind/SketchRNN_tf2

# Train the model (development phase)
To train the model on a dataset you will need to pull the docker image onto a GPU enabled docker host using docker-compose for instance

    docker-compose -f docker-compose.yml run parisApp

Make sure the training data is available in the mounted volumes 

    /home/spawn/dev/paris/aimachine:/app/dev/aimachine

Within the docker container run the train model test method. (do not forget to turn of the pytest.mark.skip)

    python -m pytest -s Tests/test_TrainModelSVGAutoEncoderFromCustom.py 

or

    python -m pytest -s Tests/test_TrainModelSVGAutoEncoderFromQuickDraw.py 




# svg path tester
http://naiksoftware.github.io/svg.html

# SVG Path Definitions
docs
https://vanseodesign.com/web-design/svg-basics/
http://vanseodesign.com/web-design/svg-paths-line-commands/
https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths

svg path tester
http://naiksoftware.github.io/svg.html
## Move command

## Line commands

## Curve commands
C x1, y1 x2, y2 x, y
- x1, y1 = control point at the beginning of the cure
- x2, y2 = control point at the end of the curve
- x, y = end point for the curve



https://dspace.mit.edu/bitstream/handle/1721.1/119692/1078149677-MIT.pdf
https://arxiv.org/pdf/1704.03477.pdf

https://github.com/googlecreativelab/quickdraw-dataset

https://github.com/magenta/magenta/blob/master/magenta/models/sketch_rnn/README.md

https://console.cloud.google.com/storage/browser/quickdraw_dataset/sketchrnn;tab=objects?pli=1&prefix=&forceOnObjectsSortingFiltering=false

https://github.com/eyalzk/sketch_rnn_keras

##### Example usage:
python VAERNN_train.py --data_dir=./Data/Sketch-rnn --data_set=cat --experiment_dir=./sketch_rnn/experiments


https://console.cloud.google.com/storage/browser/quickdraw_dataset/sketchrnn;tab=objects?prefix=&forceOnObjectsSortingFiltering=false

https://nextjournal.com/nextjournal/sketch-rnn-magenta

https://github.com/hardmaru/sketch-rnn-datasets




## A Learned Representation for Scalable Vector Graphics
The paper [A Learned Represenation for Scalable Vector Graphics]() by Raphael Gontijo Lopes, David Ha, Douglas Eck and Jonathon Shlens at Google Brain from 2019 was the first resource I used to investigate this. 

After more reading I discovered the [Sketch RNN](https://arxiv.org/abs/1704.03477) paper again by David Ha and Douglas Eck.

This paper is already implemented on different projects for instance the [Magenta Tensorflow project](https://magenta.tensorflow.org/assets/sketch_rnn_demo/index.html)

The model developed for this project is actually based on the model described in the sketch-rnn model implementation and specifically the one ported to tensorflow 2 by [stwind](https://github.com/stwind)
